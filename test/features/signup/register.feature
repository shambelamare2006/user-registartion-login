Feature: user registration to the system
        In order to log in to the system
        As a user 
        I can register to the system so that 
        I can log in to the system and user services.
@new
Scenario Outline: registeration of new user
    Given I am not a registered user
    When I submit the registration form with the following details
    |Email  |FirstName  |LastName  |Password  |
    |<Email>|<FirstName>|<LastName>|<Password>|
    Then I should get confirmation <confirmation>
    Examples:
        |            Email|  FirstName|  LastName|  Password|               confirmation|
        |   jhon@gmail.com|  jhon|       doe|        secret|     successfully registered|
        |   jane@gmail.com|  jane|       doe|        secret|     successfully registered|
        |    jhongmail.com|  jhon|       doe|        secret|               invalid input|
        |       jane@gmail|  jane|       doe|        secret|               invalid input|
        |  user1@gmail.com|  jhon|       doe|              |            incomplete input|
        |  user2@gmail.com|  jane|          |        secret|            incomplete input|
        |  user3@gmail.com|      |       doe|        secret|            incomplete input|
        |                 |  jane|       doe|        secret|            incomplete input|

@existing
Scenario Outline: registration of already registered user
    Given I am a registered user
    When I submit the registration form with the following details
    |Email  |FirstName  |LastName  |Password | 
    |<Email>|<FirstName>|<LastName>|<Password>|
    Then I should get an error saying <confirmation>
    Examples:
        |   Email|  FirstName|  LastName|   Password|              confirmation|
        |   jhon@gmail.com|  jhon|  doe|   secret|      user already registered|
        |   jane@gmail.com|  jane|  doe|   secret|      user already registered|

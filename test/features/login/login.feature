Feature: Log in to the system
        As a a registered user
        I can log in to the system so that 
        I can start using the system.
@registered
Scenario Outline: log in with correctly registered credentials
    Given I am a registered user with <email> 
    When I log in to the system with correct <email> and <password>
    Then I should get confirmation <confirmation>
    Examples:
        |   email|          password|           confirmation|
        |   jhon@gmail.com|   secret|    successfully logedin|
        |   jane@gmail.com|   secret|    successfully logedin|
        |   jhon@gmail.com|  notsecret|       wrong password|
        |   jane@gmail.com|  notsecret|       wrong password|
@unregistered
Scenario Outline: log in with Unregistered credentials
    Given I am not a registered with <email> 
    When I log in to the system with <email> and <password>
    Then I should get confirmation <confirmation>
    Examples:
        |   email|          password|          confirmation|
        |   user1@gmail.com|  secret|   user not registered|
        |   user2@gmail.com|  secret|   user not registered|

